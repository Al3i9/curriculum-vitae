import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  formulario!: FormGroup;
  mensaje!: string;
  mensajes: string = 'Formulario enviado con exito';

  constructor(private fb: FormBuilder) { 
    this.crearFormulario();
  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      mensaje: ['', [Validators.required, Validators.minLength(7)]]
    });
  }

  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }

  get correoNoValido(){
    return this.formulario.get('email')?.invalid && this.formulario.get('email')?.touched
  }

  get mensajeNoValido(){
    return this.formulario.get('mensaje')?.invalid && this.formulario.get('mensaje')?.touched
  }

  ngOnInit(): void {
  }

  enviar(): void{
    console.log(this.formulario);
    this.mensaje = this.mensajes
    this.LimpiarFormulario()
    setTimeout(() => {
      this.mensaje = ''
    }, 3000);

  }

  LimpiarFormulario(){
    this.formulario.reset();
  }


  MensajeEnviado(){
    this.mensaje = this.mensajes
  }

}
